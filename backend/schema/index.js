const _ = require("lodash");
const graphql = require("graphql");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Author = require("../models/author")
const Book = require("../models/book")
const User = require("../models/user")
const { SECRET_TOKEN } = require('../utils'); 

const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLList,
    GraphQLID,
    GraphQLString,
    GraphQLInt,
    GraphQLBoolean
} = graphql

const UserType = new GraphQLObjectType({
    name: "User",
    fields: () => ({
        id: { type: GraphQLID },
        firstName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        userName: { type: GraphQLString },
        email: { type: GraphQLString },
        password: { type: GraphQLString },
        birthDate: { type: GraphQLString },
        userPlan: { type: GraphQLString },
        userType: { type: GraphQLString },
        userCycle: { type: GraphQLString },
        newsSubscriber: { type: GraphQLBoolean },
        jwt: { type: GraphQLString }
    })
})

const BookType = new GraphQLObjectType({
    name: "Book",
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        genre: { type: GraphQLString },
        authorId: {
            type: AuthorType, 
            resolve({authorId}, args) { return Author.findById(authorId) }
        }
    })
});

const AuthorType = new GraphQLObjectType({
    name: "Author",
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        age: { type: GraphQLInt },
        books: {
            type: new GraphQLList(BookType),
            resolve({ id }, args) { return Author.findById(id) }

        }
    })
});

const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    fields: {
        currentUser: {
            type: UserType,
            resolve(parent, args, context) { return context.user }
        }, 
        users: {
            type: new GraphQLList(UserType),
            resolve(parent, args) { return User.find({})}
        },
        user: {
            type: UserType,
            args: { id: { type: GraphQLID } },
            resolve(parent, { id }) { return User.findById(id) }
        },
        books: {
            type: new GraphQLList(BookType),
            resolve(parent, args) { return Book.find({})}
        },
        book: {
            type: BookType,
            args: { id: { type: GraphQLID } },
            resolve(parent, { id }) { return Book.findById(id) }
        },
        authors: {
            type: new GraphQLList(AuthorType),
            resolve(parent, args) { return Author.find({})}
        },
        author: {
            type: AuthorType,
            args: { id: { type: GraphQLID } },
            resolve(parent, { id }) { return Author.findById(id) }
        },
    }
})

const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
        signup: {
            type: UserType,
            args: { 
                firstName: { type: GraphQLString },
                lastName: { type: GraphQLString },
                userName: { type: GraphQLString },
                email: { type: GraphQLString },
                password: { type: GraphQLString },
                birthDate: { type: GraphQLString },
                userPlan: { type: GraphQLString },
                userType: { type: GraphQLString },
                userCycle: { type: GraphQLString },
                newsSubscriber: { type: GraphQLBoolean },
            },
            async resolve(parent, args) {

                let hashPassword = await bcrypt.hash(args.password, 10)
                let existingUser = await User.findOne({ username: args.userName })
                let existingEmail = await User.findOne({ email: args.email })

                if (existingEmail) {
                    throw new Error("Email já cadastrado!")
                }
            
                if (existingUser) {
                    throw new Error("Usuário já cadastrado!")
                }

                let user = await new User({ ...args, password: hashPassword });
                
                user.jwt = jwt.sign({ id: user.id }, ...SECRET_TOKEN)
                return user.save()
            }
        },
        login: {
            type: UserType,
            args: {
                email: { type: GraphQLString },
                password: { type: GraphQLString },
            }, 
            async resolve (parent, { email, password }) {
                let user = await User.findOne({ email })

                if (!user) {
                    throw new Error ("Email não encontrado!")
                }

                let validPassword = await bcrypt.compare(password, user.password)

                if (!validPassword) {
                    throw new Error("Senha incorreta!")
                }

                user.jwt = jwt.sign({ id: user.id }, ...SECRET_TOKEN)

                return user
            }
        },
        addAuthor: {
            type: AuthorType,
            args: {
                name: { type: GraphQLString },
                age: { type: GraphQLInt },
            },
            resolve(parent, { name, age }) {
                let author = new Author({ name, age })
                return author.save()
            }
        },
        addBook: {
            type: BookType,
            args: {
                name: { type: GraphQLString },
                genre: { type: GraphQLString },
                authorId: { type: GraphQLID }
            },
            resolve(parent, { name, genre, authorId }) {
                let book = new Book({ name, genre, authorId })
                return book.save()
            }
        },
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation,

})