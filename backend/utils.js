const jwt = require('jsonwebtoken')
const SECRET_TOKEN = ['nuvemSecret', { expiresIn: '1d' }]
const User = require("./models/user");

const getUser = async (authorization) => {
    if (authorization) {
        const { ok, result } = await new Promise(resolve => {
            jwt.verify(authorization, ...SECRET_TOKEN, (err, result) => {
                err ? resolve({ ok: false, result: err }) 
                    : resolve({ ok: true, result })
            })
        });
        return ok ? await User.findOne({ _id: result.id }) : null
    }
    
    return null
};

module.exports = { SECRET_TOKEN, getUser }

