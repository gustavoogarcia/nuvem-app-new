const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: String,
    userName: String,
    email: String,
    password: String,
    birthDate: String,
    userPlan: String,
    userType: String,
    userCycle: String,
    newsSubscriber: Boolean,
    jwt: String
})

module.exports = mongoose.model("User", userSchema)

        