const { GraphQLServer } = require('graphql-yoga');
const schema = require("./schema")
const mongoose = require("mongoose")
const { getUser } = require('./utils'); 

async function context(headers) {
    const authorization = headers.request.headers.authorization
    const user = await getUser(authorization)
    return { user }
}

const server = new GraphQLServer({ 
    schema,
    context 
})

server.start( () => console.log(`Server is running on http://localhost:4000`))
mongoose.connect('mongodb://gustavo.ogarcia:gus77man@ds127342.mlab.com:27342/swapp', { useNewUrlParser: true })
mongoose.connection.once('open', () => {console.log('Connected to database')})
