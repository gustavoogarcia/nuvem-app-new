import React from "react";
import { NameContext } from '../../__helpers__/contexts';

 const Box = ({ name, children, isOpen, onClick }) => (
    <div className={`${ name } ${ isOpen ? "is-open" : "" }`}  onClick={ onClick }>
        { children }
    </div>
)   

export default props => (
    <NameContext.Consumer>
        { name => <Box {...props} name={name} />}
    </NameContext.Consumer>
)