import React from "react";
import { NameContext } from '../../__helpers__/contexts';

 const Nav = ({ children, isOpen, onClick }) => (
    <NameContext.Consumer>
        { name => (
            <nav className={ `menu__nav ${ name }__nav ${ name }__menu__nav ${ isOpen && "is-open" }` }  onClick={ onClick }>
                { children }
            </nav>
        )}  
    </NameContext.Consumer>
)   

export default Nav