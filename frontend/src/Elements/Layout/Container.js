import React from "react";
import { NameContext } from '../../__helpers__/contexts';

const Container = ({ children }) => (
    <NameContext.Consumer>
        { name => (
            <div className={`container ${name}__container`}>
                { children }
            </div>
        )}  
    </NameContext.Consumer>
)

export default Container 