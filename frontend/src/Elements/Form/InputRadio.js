import React, { Component } from 'react';

class InputRadio extends Component {
    state = {
        value: ""
    }
    
    inputChange = (name) => {
        console.log(name)
        this.setState({
            value: name
        })
    }

    render() {
        const { props, inputChange } = this
        const { name, options } = props

        return (
            <div className={`${name}__checkbox`}>
                <label>Perfil*</label>
                { options.map( ({ label, value }) => (
                    <div className={ `checkbox ${value}--checkbox` } key={value}>
                        <label htmlFor={ name }>{ label }</label>
                        <input 
                            checked={this.state.value === value}
                            type="radio" 
                            name={ name } 
                            value={ value } 
                            onClick={() => 
                            inputChange(name)}/>
                        <span className="checkmark"/>
                    </div>
                ))}
            </div>
        )
    }
}

export default InputRadio;