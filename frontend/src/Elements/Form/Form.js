import React from 'react';
import { NameContext } from '../../__helpers__/contexts';

 const Form = ({ children, onSubmit }) => (
    <NameContext.Consumer>
        { name => (
            <form 
                className={`${ name }__form`}
                onSubmit={ onSubmit }>
                { children }
            </form>
        )}
    </NameContext.Consumer>
)

export default Form

