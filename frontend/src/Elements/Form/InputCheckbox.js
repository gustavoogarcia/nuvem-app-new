import React, { Component } from 'react';

class InputCheckbox extends Component {
    state = {
        value: ""
    }
    
    inputChange = (e) => {
        this.setState({
            value: e.target.value
        })
    }

    render() {
        const { state, props, inputChange } = this
        const { name, label } = props
        const { value } = state

        return (
            <div className={`${name}__checkbox`}>
                <label htmlFor={ name }>{ label }</label>
                <input 
                    type="checkbox" 
                    name={ name } 
                    value={ value } 
                    defaultChecked={value}
                    onChange={(e) => inputChange(e, name)}/>
                <span className="checkmark"></span>
            </div>
        )
    }
}
        
export default InputCheckbox;
