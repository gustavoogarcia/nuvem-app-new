import React, { Component, Fragment } from 'react';

class InputText extends Component {
    state = {
        value: ""
    }
    
    inputChange = (e) => {
        this.setState({
            value: e.target.value
        })
    }

    render() {
        const { type, name, label, placeholder } = this.props
        const { value } = this.state

        return (
            <Fragment>
                <label htmlFor={ name }>{ label }</label>
                <input 
                    onChange={ (e) => this.inputChange(e) } 
                    value={ value } 
                    type={ type } 
                    name={ name } 
                    placeholder={ placeholder } >
                </input> 
            </Fragment>
        )
    }
}

export default InputText;