import React from 'react';
import { Link } from 'react-router-dom'
import { NameContext } from '../__helpers__/contexts';
import logo from "../__links__/imgs/nuvem-logo.png";

const Logo = () => (
    <NameContext.Consumer>
        { name => (
            <Link to="/"><img className={`logo ${name}__logo`} src={ logo } alt="Nuvem de Estudos Logo"/></Link>
        )}  
    </NameContext.Consumer>
);

export default Logo;