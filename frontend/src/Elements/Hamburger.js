import React from 'react';
import { NameContext, DropdownContext } from '../__helpers__/contexts';
import Box from '../Elements/Layout/Box';

const Hamburguer = ({ isOpen, dropdown }) => (
    <DropdownContext.Consumer>
        { ({ dropdownChange }) => (
            <NameContext.Consumer>
                { name => (
                    <NameContext.Provider value={ `hamburger ${name}__hamburger` }>
                        <Box isOpen={ isOpen } onClick={ (e) => dropdownChange(e, dropdown) }>
                            <div className="hamburger__outer" >
                                <div className="hamburger__inner"></div>
                            </div>
                        </Box>
                    </NameContext.Provider>
                )}  
            </NameContext.Consumer>
        )}  
    </DropdownContext.Consumer>
)

export default Hamburguer