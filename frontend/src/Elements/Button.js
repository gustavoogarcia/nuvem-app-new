import React from "react";

const Button = ({ name, label, onClick, className}) => (
    <button className={`button--${name} button--${className}`} onClick={onClick} >{label }</button>
)

export default Button