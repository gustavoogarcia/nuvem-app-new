import React from 'react';
import { Link } from 'react-router-dom'
import { NameContext } from '../__helpers__/contexts';
import Box from '../Elements/Layout/Box'
import Container from '../Elements/Layout/Container'
import Bubbles from './Bubbles'

const TryBanner = () => (
    <NameContext.Provider value={ "try-banner" }>
        <Box>
            <Container>
                <Bubbles side="left"/>
                    <NameContext.Consumer>
                        { name => (
                            <NameContext.Provider value={`${name}__message`}>
                                <Box>
                                    <h2>Experimente a nuvem de estudos</h2>
                                    <p>Uma plataforma de aprendizagem digital que possibilita lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquam sem sit amet nibh ultricies porta nec id purus. Nulla non venenatis turpis, at convallis neque. Ut accumsan ex eget odio mattis ultrices. Suspendisse non elit in elit vulputate tincidunt sed.</p>
                                    <Link to="/registro"><button className="button--blue">Cadastre-se gratuitamente</button></Link>
                                </Box>
                            </NameContext.Provider>
                        )}
                    </NameContext.Consumer>
                <Bubbles side="right"/>
            </Container>
        </Box>
    </NameContext.Provider>
)

export default TryBanner

