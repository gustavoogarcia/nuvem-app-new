import React from 'react';
import { NameContext, DropdownContext } from '../../__helpers__/contexts';
import Container from '../../Elements/Layout/Container'
import Box from '../../Elements/Layout/Box'
import Button from '../../Elements/Button'
import SigninForm from '../SigninForm';

const HeaderSignIn = () => (
    <DropdownContext.Consumer>
        { ({ HeaderSignin, dropdownChange }) => (
            <NameContext.Consumer>
                { name => (
                    <NameContext.Provider value={`${name}__signin`}>
                        <Box isOpen={ HeaderSignin }>
                            <Container>
                                <SigninForm/>
                                <Button 
                                    name="close" 
                                    label="X" 
                                    className="red" 
                                    onClick={ (e) => dropdownChange(e, "HeaderSignin") }/>
                            </Container>
                        </Box>
                    </NameContext.Provider>
                )}
            </NameContext.Consumer> 
        )}
    </DropdownContext.Consumer>  
)

export default HeaderSignIn