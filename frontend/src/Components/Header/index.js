import React from 'react';
import Container from '../../Elements/Layout/Container'
import SocialIcons from '../SocialIcons';
import HeaderSignIn from './HeaderSignin';
import Hamburguer from '../../Elements/Hamburger';
import Logo from '../../Elements/Logo';
import NavMenu from '../Menus/NavMenu';
import UserMenu from '../Menus/UserMenu';

const Header = ({currentUser, HeaderMenu}) => (
    <header>
        { !currentUser 
            && <HeaderSignIn/> }
        <Container>
            <SocialIcons/>
            <Hamburguer isOpen={ HeaderMenu } dropdown="HeaderMenu"/>
            <Logo/>
        </Container>
        { !currentUser 
            ? <NavMenu isOpen={HeaderMenu}/>
            : <UserMenu isOpen={HeaderMenu}/>
        }
    </header>
)

export default Header