import React, { Component, Fragment } from 'react';
import gql from "graphql-tag";
import { graphql, compose } from "react-apollo";
import { UserContext } from '../__helpers__/contexts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Form from '../Elements/Form/Form';
import InputText from '../Elements/Form/InputText';
import InputRadio from '../Elements/Form/InputRadio';
import InputCheckbox from '../Elements/Form/InputCheckbox';
import { withRouter } from 'react-router-dom'

class SigninForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            register: true,
        }
        this.signup = this.signup.bind(this)
        this.login = this.login.bind(this)
    }

    componentDidMount() {
        this.setState ({ register: this.props.register })
    }

    inputChange = (e, input) => {
        this.setState({
            [input]: e.target.value
        })
    }
    
    signup = (e, refetch) => {
        e.preventDefault()
        this.props.signup({
            variables: {
                email: e.target.querySelector('input[name="email"]').value,
                password: e.target.querySelector('input[name="password"]').value,
                birthDate: e.target.querySelector('input[name="birthDate"]').value,
                userName: e.target.querySelector('input[name="userName"]').value,
                userType: e.target.querySelector('input[name="userType"]').value,
            }
        }).then( res => {
            this.saveUserData(res, refetch)
        })
    }

    login = (e, refetch) => {
        e.preventDefault()
        this.props.login({
            variables: {
                email: e.target.querySelector('input[name="email"]').value,
                password: e.target.querySelector('input[name="password"]').value,
            }
        }).then( res => {
            this.saveUserData(res, refetch)

        })
    }

    saveUserData = (res, refetch) => {
        const { jwt } = this.state.register ? res.data.signup : res.data.login
        window.localStorage.setItem("TOKEN", jwt)
        refetch()
        this.props.history.push('/')
    }


    render() {
        const { state, signup, login } = this
        const { register } = state
        const component = "register"
        const userTypes =  [
            { label: "Aluno", value: "student" },
            { label: "Professor", value: "teacher" },
            { label: "Família", value: "family" }
        ]
        
        return (
            <UserContext.Consumer>
                { data => {
                    const { refetch } = data
                    return (
                        <Form onSubmit={ register ? (e) =>  signup(e, refetch) : (e) => login(e, refetch) }>
                            <h2>{register ? "Cadastrar" : "Entrar"}</h2>
                            <button type="button" className='button--social button--facebook'>
                                <FontAwesomeIcon icon={['fab', 'facebook-f']} />
                                Entrar com o Facebook
                            </button>
                            <button type="button" className='button--social button--google'>
                                <FontAwesomeIcon icon={['fab', 'google']} />
                                Entrar com o Google
                            </button>
                            <InputText type="text" name="email" label="Email*"/>
                            <InputText type="password" name="password" label="Senha*"/>
                            {register && (
                                <Fragment>
                                    <InputText type="password" name="rePassword" label="Confirmar senha*"/>
                                    <InputText type="number" name="birthDate" label="Data de nascimento*"/>
                                    <InputText type="text" name="userName" label="Nome de usuário*" />
                                    <InputRadio name="userType" options={ userTypes } />
                                    <InputCheckbox name="newsletter" label={`Desejo receber as novidades.`} value="email"/>
                                    <p>Clicando em "Cadastrar", você concorda<br/>com os nossos <a>termos de uso</a>.</p>
                                </Fragment>
                            )}
                            <button type="submit" className="button--submit button--blue" >{register ? 'Cadastrar' : 'Entrar'}</button>
                            <p>{ register ? "Já possui cadastro?" : "Ainda não possui um cadastro?" } <a onClick={ () => this.setState ({ register: !register }) }>clique aqui</a>.</p>
                        </Form> 
                    )
                }}
            </UserContext.Consumer>                 
        )
    }
}

const Signup = gql`
    mutation signup ($email: String!, $password: String!, $userName: String!) {
        signup(email: $email, password: $password, userName: $userName) {
            jwt
        }
    }
`

const Login = gql`
    mutation login ($email: String!, $password: String!) {
        login(email: $email, password: $password) {
            jwt
        }
    }
`

export default compose(
    graphql(Signup, { name: 'signup' }),
    graphql(Login, { name: 'login' })
)(withRouter(SigninForm))