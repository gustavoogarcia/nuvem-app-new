import React from 'react';
import { UserContext, DropdownContext } from '../../__helpers__/contexts';
import { Link } from 'react-router-dom'
import Nav from '../../Elements/Layout/Nav'

const unloadUser = (refetch) => {
    window.localStorage.removeItem("TOKEN")
    refetch()
}

const UserMenu = ({isOpen}) => (
    <DropdownContext.Consumer>
        { ({ dropdownChange }) => (
            <UserContext.Consumer>
                { data => {
                    const { currentUser, refetch } = data
                    return (
                        <Nav isOpen={isOpen}>
                            <ul>
                                <li className='greetings'>{`Olá ${(currentUser) && currentUser.userName}!`}</li>
                                <li><Link to="/atividades">Atividades</Link></li>
                                <li><Link to="/perfil">Minha conta</Link></li>
                                <li onClick={ (e) => { unloadUser(refetch); dropdownChange(e, "HeaderMenu") } }>Sair</li>
                                <li className='premium-call'>Seja premium</li>
                            </ul>
                        </Nav>
                    )
                }}
            </UserContext.Consumer>
        )}
    </DropdownContext.Consumer>
)

export default UserMenu