import React from 'react';
import { Link } from 'react-router-dom'
import { UserContext, NameContext, DropdownContext } from '../../__helpers__/contexts';
import Nav from '../../Elements/Layout/Nav'

const unloadUser = (refetch) => {
    window.localStorage.removeItem("TOKEN")
    refetch()
}

const NavMenu = ({isOpen}) => (
    <DropdownContext.Consumer>
        { ({ dropdownChange }) => (
            <NameContext.Consumer>
                { name => (
                <Nav isOpen={isOpen}>
                        <UserContext.Consumer>
                            { data => {
                                const { currentUser, refetch } = data
                                return (
                                    <ul>
                                        <li><Link to="/quemsomos">Quem Somos</Link></li>
                                        <li><Link to="/comofunciona">Como funciona</Link></li>
                                        <li><Link to="/contato">Contato</Link></li>
                                        { !currentUser
                                            ? <li onClick={ (e) => dropdownChange(e, "HeaderSignin") }>Entrar</li>
                                            : <li onClick={ () => unloadUser(refetch) }>Sair</li>
                                        }    
                                    </ul>
                                )
                            }}
                        </UserContext.Consumer>
                    </Nav>
                )}
            </NameContext.Consumer>
        )}
    </DropdownContext.Consumer>
)

export default NavMenu