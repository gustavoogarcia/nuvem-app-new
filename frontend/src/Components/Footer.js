import React from 'react';
import { NameContext, DropdownContext } from '../__helpers__/contexts';
import Container from '../Elements/Layout/Container';
import SocialIcons from './SocialIcons';
import Hamburger from '../Elements/Hamburger';
import NavMenu from './Menus/NavMenu';

const Footer = () => (
    <DropdownContext.Consumer>
        { ({ FooterMenu, dropdownChange }) => (
            <NameContext.Provider value={ "footer" }>
                <footer>
                    <Container>
                        <div className="footer__info">
                            <h6>Contato</h6>
                            <p>contato@nuvemdeestudos.com.br</p>
                            <p>+55 11 0000-0000</p>
                            <SocialIcons/>
                            <Hamburger isOpen={ FooterMenu } dropdown="FooterMenu"/>
                        </div>
                        <NavMenu isOpen={ FooterMenu } dropdownChange={ dropdownChange }/>
                    </Container>
                </footer>
            </NameContext.Provider>
        )}
    </DropdownContext.Consumer>
)
export default Footer