import React from 'react';
import { NameContext } from '../__helpers__/contexts';
import Box from '../Elements/Layout/Box'
import '../__links__/fonts/icomoon/icomoon.css'

const Bubbles = (props) => {
    let bubbles = [];

    switch (props.side) {
        case ("left") :
            bubbles = [
                {color: "blue", icon: "pen", size: "xxg"}, 
                {color: "pink", icon: "no-icon", size: "xs"}, 
                {color: "orange", icon: "no-icon", size: "sm"}, 
                {color: "light-green", icon: "clock", size: "xg"}, 
                {color: "pink", icon: "magnifying", size: "xg"}, 
                {color: "green", icon: "chemical", size: "lg"}, 
                {color: "yellow", icon: "atom", size: "lg"}, 
                {color: "purple", icon: "book", size: "xxg"}, 
                {color: "blue", icon: "no-icon", size: "sm"}, 
                {color: "light-green", icon: "chemical2", size: "md"},
                {color: "orange", icon: "no-icon", size: "xs"}, 
            ]; break;
        case ("right") :
            bubbles = [
                {color: "green", icon: "ball", size: "lg"}, 
                {color: "pink", icon: "no-icon", size: "sm"}, 
                {color: "yellow", icon: "calculator", size: "xg"}, 
                {color: "purple", icon: "no-icon", size: "xs"}, 
                {color: "yellow", icon: "no-icon", size: "md"}, 
                {color: "blue", icon: "cell", size: "xg"}, 
                {color: "orange", icon: "glasses", size: "xxg"}, 
                {color: "light-green", icon: "no-icon", size: "sm"}, 
                {color: "purple", icon: "chemical3", size: "lg"}, 
                {color: "pink", icon: "book2", size: "lg"}, 
                {color: "green", icon: "hive", size: "lg"}, 
                {color: "yellow", icon: "no-icon", size: "sm"}, 
            ]; break;
        default: break;
    }

    return (
        <NameContext.Provider value={ "bubbles" }>
            <Box>
                {bubbles.map( ({ color, icon, size }) => (
                    <div key={ color + icon + size } className={`bubble ${color} ${icon} ${size} icon-${icon}`}></div>
                ))}
            </Box>
        </NameContext.Provider>    
    )
}

export default Bubbles

