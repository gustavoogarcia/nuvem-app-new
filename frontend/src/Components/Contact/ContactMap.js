import React from 'react';
import { NameContext } from '../../__helpers__/contexts';
import Box from '../../Elements/Layout/Box'
import map from '../../__links__/imgs/contact-map.png'

const ContactMap = () => (
    <NameContext.Consumer>
        { name => (
            <NameContext.Provider value={`${name}-map`}>
                <Box>
                    <img src={map} alt={`${name}-map`}/>
                </Box>
            </NameContext.Provider>
        )}
    </NameContext.Consumer>
)

export default ContactMap;