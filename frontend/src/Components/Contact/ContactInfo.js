import React from 'react';
import { NameContext } from '../../__helpers__/contexts'
import Box from '../../Elements/Layout/Box'
import SocialIcons from '../SocialIcons';

const ContactInfo = () => (
    <NameContext.Consumer>
        { name => (
            <NameContext.Provider value={`${name}-info`}>
                <Box>
                    <h2>Entre em contato</h2>
                    <p>Rua Vergueiro 3307 - Sala 8</p>
                    <p>Vila Mariana - São Paulo SP</p>
                    <p>CEP 04101 300</p>
                    <p>Tel.: 11 9000 0000</p>
                    <a src="mailto:contato@nuvemdeestudos.com.br">contato@nuvemdeestudos.com.br</a>
                    <SocialIcons />
                </Box>
            </NameContext.Provider>
        )}
    </NameContext.Consumer>
)

export default ContactInfo;