import React from 'react';
import Box from '../../Elements/Layout/Box'
import Form from '../../Elements/Form/Form';
import InputText from '../../Elements/Form/InputText';

const ContactForm = () => (
    <Form>
        <InputText type="text" name="name" label="Nome Completo*"/>
        <InputText type="email" name="email" label="E-mail*"/>
        <InputText type="number" name="phone" label="Telefone*"/>
        <div className="input message-input">
            <label htmlFor="message">Mensagem*</label>
            <textarea rows="4" name="message"/>
        </div>
        <p>*Campos Obrigatórios</p>
        <Box>
            <button className="orange-button" >Enviar</button>
            <button className="gray-button" >Limpar</button>
        </Box>       
    </Form>
)

export default ContactForm;