import React from 'react';
import { NameContext } from '../__helpers__/contexts';
import Box from '../Elements/Layout/Box';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default () => (
    <NameContext.Consumer>
        { name => (
            <NameContext.Provider value={`social-icons ${name}__social-icons`}>
                <Box>
                    <FontAwesomeIcon icon={['fab', 'linkedin']} />
                    <FontAwesomeIcon icon={['fab', 'youtube-square']} />
                    <FontAwesomeIcon icon={['fab', 'whatsapp']} />
                    <FontAwesomeIcon icon={['fab', 'facebook']} />
                </Box>
            </NameContext.Provider>
        )}
    </NameContext.Consumer>  
)