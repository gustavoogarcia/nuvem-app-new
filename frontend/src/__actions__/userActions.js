export const signup = (e, refetch) => {
    e.preventDefault()
    this.signup({
        variables: {
            register: false,
            email: e.target.querySelector('input[name="email"]').value,
            password: e.target.querySelector('input[name="password"]').value,
            birthDate: e.target.querySelector('input[name="birthDate"]').value,
            userName: e.target.querySelector('input[name="userName"]').value,
            userType: e.target.querySelector('input[name="userType"]').value,
        }
    }).then( res => {
        this.saveUserData(res, refetch)
    })
}

export const login = (e, refetch) => {
    e.preventDefault()
    this.login({
        variables: {
            email: e.target.querySelector('input[name="email"]').value,
            password: e.target.querySelector('input[name="password"]').value,
        }
    }).then( res => {
        this.saveUserData(res, refetch)

    })
}

export const saveUserData = (res, refetch) => {
    const { jwt } = this.state.register ? res.data.signup : res.data.login
    window.localStorage.setItem("TOKEN", jwt)
    refetch()
    this.props.history.push('/')
}