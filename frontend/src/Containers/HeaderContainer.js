import React from "react"
import { UserContext, DropdownContext, NameContext } from '../__helpers__/contexts';
import Header from "../Components/Header"

const HeaderContainer = (props) => (
    <NameContext.Provider value="header">
        <UserContext.Consumer>
            { ({ currentUser }) => (
                <DropdownContext.Consumer>
                    { ({ HeaderMenu }) => (
                        <Header {...props} currentUser={ currentUser } HeaderMenu={ HeaderMenu } />
                    )}
                </DropdownContext.Consumer>
            )}
        </UserContext.Consumer>
    </NameContext.Provider>
);

export default HeaderContainer