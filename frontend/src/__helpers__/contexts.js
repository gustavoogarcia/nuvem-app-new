import React from 'react';

export const UserContext =  React.createContext(null);
export const NameContext =  React.createContext(null);
export const DropdownContext =  React.createContext(null);