import React, { Component } from 'react';
import gql from "graphql-tag";
import { graphql, compose } from "react-apollo";
import { Switch, Route } from 'react-router-dom';
import { UserContext, DropdownContext } from './__helpers__/contexts';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import Header from './Containers/HeaderContainer';
import RegisterPage from './Pages/RegisterPage';
import Footer from './Components/Footer';
import HomePage from './Pages/HomePage';
import ContactPage from './Pages/ContactPage';
import AboutPage from './Pages/AboutPage';
import HowWorksPage from './Pages/HowWorksPage';
import ActivitiesPage from './Pages/ActivitiesPage';
import ProfilePage from './Pages/ProfilePage';
import './App.css'; 

library.add(fab);

class App extends Component {
	state = {
		dropdowns: {
			HeaderSignin: false,
			HeaderMenu: false,
			FooterMenu: false,
			dropdownChange: (e, dropdown) => {
				let { dropdowns } = this.state
				let { HeaderSignin, HeaderMenu, FooterMenu } = dropdowns
				e.preventDefault();
				switch (dropdown) {
					case "HeaderSignin":
						this.setState ({ dropdowns: { ...dropdowns, HeaderSignin: !HeaderSignin } })
						break;
					case "HeaderMenu":
						this.setState ({ dropdowns: { ...dropdowns, HeaderMenu: !HeaderMenu } })
						break;
					case "FooterMenu":
						this.setState ({ dropdowns: { ...dropdowns, FooterMenu: !FooterMenu } })
						break;
					default: break;
				}
			}
		}
	}

	render() {
		const { data } = this.props
		const { dropdowns } = this.state

		return (
			<DropdownContext.Provider value={ dropdowns }>
				<UserContext.Provider value={ data }>
					<Header/>
					<Switch>
						<Route path="/" exact render={ () => <HomePage /> } />
						<Route path="/registro" render={ () => <RegisterPage /> } />
						<Route path="/quemsomos" render={ () => <AboutPage /> } />
						<Route path="/comofunciona" render={ () => <HowWorksPage /> } />
						<Route path="/contato" render={ () => <ContactPage /> } />
						<Route path="/atividades" render={ () => <ActivitiesPage /> } />
						<Route path="/perfil" render={ () => <ProfilePage /> } />
					</Switch>
					<Footer dropdownChange={this.dropdownChange}/>
				</UserContext.Provider>
			</DropdownContext.Provider>
		)
	}
}  

const Query = gql`
	{
		currentUser{
			id
  			userName
		}
	}
`

export default compose(graphql(Query, { name: "data" }))(App)
