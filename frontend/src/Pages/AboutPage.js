import React from 'react';
import { NameContext } from '../__helpers__/contexts';
import Page from '../Elements/Layout/Page'
import Container from '../Elements/Layout/Container'
import Bubbles from '../Components/Bubbles';
import Box from '../Elements/Layout/Box';

const AboutPage = () => (
    <NameContext.Provider value={ "about" }>
        <Page>
            <Container>
                <Bubbles side="left"/>
                <Box>
                    <h1>Quem Somos</h1>
                </Box>
                <Bubbles side="right"/>
            </Container>
        </Page> 
    </NameContext.Provider>
)

export default AboutPage;