import React, { Component } from 'react';
import gql from "graphql-tag";
import { graphql, compose } from "react-apollo";
import { NameContext } from '../__helpers__/contexts';
import Page from '../Elements/Layout/Page'
import Container from '../Elements/Layout/Container'
import Bubbles from '../Components/Bubbles';
import Box from '../Elements/Layout/Box';
import InputText from '../Elements/Form/InputText'
import InputRadio from '../Elements/Form/InputRadio'
import InputCheckbox from '../Elements/Form/InputCheckbox'

class ProfilePage extends Component {
    state = {
        userName: "",
        email: "",
        password: "",
        rePassword: "",
        birthDate: "",
        userType: "",
        newsSubscriber: "",
    }

    componentWillReceiveProps() {
        this.setState({ ...this.state, ...this.props.data.currentUser })
    }

    inputChange = (e, input) => {
        this.setState({
            [input]: e.target.value
        })
    }


    render() {
        const { state, inputChange,  } = this
        const { userName, email, password, rePassword, birthDate, userType, newsSubscriber } = state
        const component = "profile"
        const userTypes =  [
            { label: "Aluno", value: "student" },
            { label: "Professor", value: "teacher" },
            { label: "Família", value: "family" }
        ]

        return (
            <NameContext.Provider value={ "profile" }>
                <Page>
                    <Container>
                        <Bubbles side="left"/>
                            <Box>
                                <h2>Minha Conta</h2>
                                <p>@{userName}</p>
                                <InputText 
                                    inputChange={ inputChange }
                                    type="text" 
                                    name="email" 
                                    label="Email*" 
                                    value={ email }/>
                                <InputText 
                                    inputChange={ inputChange }
                                    type="password" 
                                    name="password" 
                                    label="Digite uma nova senha*" 
                                    value={ password }/>
                                <InputText 
                                    inputChange={ inputChange }
                                    type="password" 
                                    name="rePassword" 
                                    label="Confirme sua nova senha*" 
                                    value={ rePassword }/>
                                <InputText 
                                    inputChange={ inputChange }
                                    type="number" 
                                    name="birthDate" 
                                    label="Data de nascimento*" 
                                    value={birthDate}/>
                                <div className={`${component}__radio`}>
                                    <label>Perfil*</label>
                                    { userTypes.map( ({ label, value }) => (
                                        <InputRadio
                                            inputChange={ inputChange }
                                            key={ value } 
                                            name="userType" 
                                            label={ label } 
                                            value={ value }
                                            checked={ userType }/>
                                    ))}
                                </div>
                                <div className={`${component}__checkbox`}>
                                    <InputCheckbox 
                                        inputChange={ inputChange }
                                        name="newsletter" 
                                        label={`Desejo receber as novidades.`} 
                                        value="email"
                                        checked={ newsSubscriber }/>
                                </div>
                            </Box>
                        <Bubbles side="right"/>
                    </Container>
                </Page> 
            </NameContext.Provider>
        )
    }
}

const Query = gql`
	{
		currentUser{
			id
  			userName
            email
            birthDate
            userPlan
            userType
            userCycle
            newsSubscriber
		}
	}
`

export default compose(graphql(Query, { name: "data" }))(ProfilePage)