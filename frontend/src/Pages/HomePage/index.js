import React from 'react';
import { NameContext, UserContext } from '../../__helpers__/contexts';
import UnloggedHome from './UnloggedHome';
import LoggedHome from './LoggedHome';
import Page from '../../Elements/Layout/Page'

const HomePage = () => (
    <UserContext.Consumer>
        { ({ currentUser }) => (
            <NameContext.Provider value={ "home" }>
                <Page>
                    { !currentUser ? <UnloggedHome/> : <LoggedHome/> }
                </Page> 
            </NameContext.Provider>
        )}
    </UserContext.Consumer>
)

export default HomePage