import React from 'react';
import { NameContext } from '../__helpers__/contexts';
import Page from '../Elements/Layout/Page'
import Container from '../Elements/Layout/Container'
import ContactInfo from '../Components/Contact/ContactInfo';
import ContactForm from '../Components/Contact/ContactForm';
import ContactMap from '../Components/Contact/ContactMap';

const ContactPage = () => (
    <NameContext.Provider value={ "contact" }>
        <Page>
            <Container>
                <ContactInfo/>
                <ContactForm/>
            </Container>
            <ContactMap/>
        </Page> 
    </NameContext.Provider>
)

export default ContactPage;