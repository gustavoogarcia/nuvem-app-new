import React from 'react';
import { NameContext } from '../__helpers__/contexts';
import Page from '../Elements/Layout/Page'
import Container from '../Elements/Layout/Container'
import Box from '../Elements/Layout/Box';
import SigninForm from '../Components/SigninForm';

const Register = () => (
    <NameContext.Provider value={ "register" }>
        <Page>
            <Container>
                <Box>
                    <SigninForm register={ true }/>          
                </Box>
            </Container>
        </Page>
    </NameContext.Provider>
)

export default Register