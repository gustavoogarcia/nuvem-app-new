import React from 'react';
import { NameContext } from '../__helpers__/contexts';
import Page from '../Elements/Layout/Page'
import Container from '../Elements/Layout/Container'
import Bubbles from '../Components/Bubbles';
import Box from '../Elements/Layout/Box';

const ActivitiesPage = () => (
    <NameContext.Provider value={ "activities" }>
        <Page>
            <Container>
                <Bubbles side="left"/>
                    <Box>
                        <h1>Atividades</h1>
                    </Box>
                <Bubbles side="right"/>
            </Container>
        </Page> 
    </NameContext.Provider>
)

export default ActivitiesPage;