import React from 'react';

export const UserContext =  React.createContext(null);
export const NameContext =  React.createContext(null);
export const DropdownContext =  React.createContext({
    
});

function dropdownChange (e, location) {
    let { HeaderMenuisOpen, SignInIsOpen, FooterMenuisOpen } = this.state
    e.preventDefault();
    switch (location) {
        case "SignIn":
            SignInIsOpen: !SignInIsOpen
            break;
        case "HeaderMenu":
            HeaderMenuisOpen: !HeaderMenuisOpen
            break;
        case "FooterMenu":
            FooterMenuisOpen: !FooterMenuisOpen
            break;
        default: break;
    }
}