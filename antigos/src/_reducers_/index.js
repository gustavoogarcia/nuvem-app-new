import { combineReducers } from 'redux'
import { LOAD_USER, UNLOAD_USER, CHANGE_PLAN } from '../_actions_'

const USER_STATE = { 
    name: "guest", 
    userPlan: "visitor",
    userType: "undefined" 
}

function userState (state = USER_STATE, action) {
    let { type, user } = action
    
    switch (type) {
        case LOAD_USER:
            return { ...state, name: user.name , userPlan: user.userPlan, userType: user.userType }
            case UNLOAD_USER:
            return { ...state, name: user.name , userPlan: user.userPlan, userType: user.userType }
        case CHANGE_PLAN:
            return { ...state, userPlan: user.userPlan }
        default:
            return state
    }
}

export default combineReducers({ user: userState });