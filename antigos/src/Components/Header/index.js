import React, { Component } from 'react';
import dropdownChange from '../../_actions_/dropdownChange';
import { NameContext, UserContext } from '../../__helpers__/context';
import Container from '../../Elements/Layout/Container';
import HeaderSignIn from './HeaderSignIn';
import SocialIcons from '../SocialIcons';
import Hamburger from '../../Elements/Hamburger';
import Logo from '../../Elements/Logo';
import NavMenu from '../Menus/NavMenu';
import UserMenu from '../Menus/UserMenu';

class Header extends Component {
    state = {
        HeaderMenuisOpen: false,
    }

    dropdownChange = dropdownChange.bind(this);

    render() {
        let { dropdownChange, props } = this;
        let { isOpen } = props;
        let { HeaderMenuisOpen  } = this.state;    

        return (
            <NameContext.Provider value="header">
                <UserContext.Consumer>
                    { user => (
                        <header>
                            { !user
                                && <HeaderSignIn isOpen={ isOpen } dropdownChange={ props.dropdownChange }/> }                    
                            <Container>
                                <SocialIcons/>
                                <Hamburger isOpen={ HeaderMenuisOpen } dropdownChange={ dropdownChange } location={"HeaderMenu"}/>
                                <Logo/>
                            </Container>
                            { !user
                                ? <NavMenu isOpen={ HeaderMenuisOpen } dropdownChange={ props.dropdownChange }/> 
                                : <UserMenu isOpen={ HeaderMenuisOpen }/> }
                        </header>
                    )}
                </UserContext.Consumer>
            </NameContext.Provider>
        )
    }
}

export default Header