import React from 'react';
import { loadUser } from '../../_actions_'
import { NameContext } from '../../__helpers__/context';
import Container from '../../Elements/Layout/Container'
import Box from '../../Elements/Layout/Box'
import Form from '../../Elements/Form/Form'
import InputText from '../../Elements/Form/InputText'
import Button from '../../Elements/Button'

const HeaderSignIn = ({ isOpen, dropdownChange }) => (
        <NameContext.Consumer>
            { name => (
                <NameContext.Provider value={`${name}__signin`}>
                    <Box isOpen={isOpen}>
                        <Container>
                            <Form>
                                <InputText type="email" name="email" label="Email" placeholder="Coloque seu email"/>
                                <InputText type="password" name="password" label="Senha" placeholder="Coloque sua senha"/>
                                <Button name="submit" label="Entrar" className="blue" onClick={ (e) => { loadUser(e); dropdownChange(e, "SignIn") } }/>
                                <Button name="close" label="X" className="red" onClick={ (e) => dropdownChange(e, "SignIn") }/>
                            </Form>
                        </Container>
                    </Box>
                </NameContext.Provider>
            )}
        </NameContext.Consumer>  
    )

export default HeaderSignIn