import React from 'react';
import { Link } from 'react-router-dom'
import { UserContext, NameContext } from '../../__helpers__/context';

const NavMenu = ({ isOpen, dropdownChange }) => {
    
    return (
        <NameContext.Consumer>
            { name => (
                <nav className={`menu__nav ${ name }__nav ${ name }__menu__nav ${ isOpen ? "is-open" : "" }`}>
                    <UserContext.Consumer>
                        { user => (
                            <ul>
                                <li><Link to="/quemsomos">Quem Somos</Link></li>
                                <li><Link to="/comofunciona">Como funciona</Link></li>
                                <li><Link to="/contato">Contato</Link></li>
                                { !user
                                    ? <li onClick={ (e) => dropdownChange(e, "SignIn") }>Entrar</li>
                                    : <li onClick={ (e) => e }>Sair</li> 
                                }    
                            </ul>
                        )}
                    </UserContext.Consumer>
                </nav>
            )}
        </NameContext.Consumer>
    )
}

export default NavMenu