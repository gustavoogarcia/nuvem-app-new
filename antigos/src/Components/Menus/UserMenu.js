import React from 'react';
import { UserContext, NameContext } from '../../__helpers__/context';
import { Link } from 'react-router-dom'
import Nav from '../../Elements/Layout/Nav'

const UserMenu = (props) => {
    const { isOpen } = props

    return (
        <UserContext.Consumer>
            { user => (
                <Nav isOpen={isOpen}>
                    <ul>
                        <li className='greetings'>{`Olá ${user.userName}!`}</li>
                        <li><Link to="/atividades">Atividades</Link></li>
                        <li><Link to="/perfil">Minha conta</Link></li>
                        <li onClick={ (e) => e }>Sair</li>
                        { user.userPlan !== "premium"
                            && <li className='premium-call'>Seja premium</li> }
                    </ul>
                </Nav>
            )}
        </UserContext.Consumer>
    )
}

export default UserMenu