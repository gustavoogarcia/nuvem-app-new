export const LOAD_USER = 'LOAD_USER'
export const UNLOAD_USER = 'UNLOAD_USER'
export const CHANGE_PLAN = 'CHANGE_PLAN'

export function loadUser (e) {
    e.preventDefault()
    return {
        type: LOAD_USER,
        user: {
            name: "Gustavo", 
            userPlan: "free",
            userType: "student"
        }
    }   
}

export function unloadUser (e) {
    e.preventDefault()
    return {
        type: UNLOAD_USER,
        user: {
            name: "guest", 
            userPlan: "visitor",
            userType: "undefined"
        }
    }   
}

export function changePlan (e) {
    e.preventDefault()
    return {
        type: CHANGE_PLAN,
        user: {
            userPlan: "premium",
        }
    }   
}