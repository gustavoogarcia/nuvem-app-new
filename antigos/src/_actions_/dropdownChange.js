export function dropdownChange (e, location) {
    let { HeaderMenuisOpen, SignInIsOpen, FooterMenuisOpen } = this.state
    e.preventDefault();
    switch (location) {
        case "SignIn":
            this.setState ({ SignInIsOpen: !SignInIsOpen })
            break;
        case "HeaderMenu":
            this.setState ({ HeaderMenuisOpen: !HeaderMenuisOpen })
            break;
        case "FooterMenu":
            this.setState ({ FooterMenuisOpen: !FooterMenuisOpen })
            break;
        default: break;
    }
}

export default dropdownChange;