import React, { Component } from 'react';
import { UserContext } from './__helpers__/context';
import { Route } from 'react-router-dom';
import gql from "graphql-tag";
import { graphql, compose } from "react-apollo";
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import dropdownChange from './_actions_/dropdownChange';
import Header from './Components/Header';
// import Footer from './Components/Footer';
// import HomePage from './Pages/HomePage';
// import ContactPage from './Pages/ContactPage';
import RegisterPage from './Pages/RegisterPage';
// import AboutPage from './Pages/AboutPage';
// import HowWorksPage from './Pages/HowWorksPage';
// import ActivitiesPage from './Pages/ActivitiesPage';
// import ProfilePage from './Pages/ProfilePage';

library.add(fab);

class App extends Component {
    
    state = {
        SignInIsOpen: false,
    }

    dropdownChange = dropdownChange.bind(this);

    render() {
        const { user } = this.props.data
        let { SignInIsOpen } = this.state
        console.log(this.props.data)

        return (
            <UserContext.Provider user={user}>
                {console.log(user)}
                <Header SignInIsOpen={SignInIsOpen} dropdownChange={this.dropdownChange}/>
                <Route path="/"  render={ () => <RegisterPage /> } />
            </UserContext.Provider>
        )
    }
                
};

const Query = gql`
    {
        currentUser { 
            userName
            userPlan
        }
    }
`
export default compose(graphql(Query, { name: "teste" }))(App)

// 
// <Route path="/quemsomos"  render={ () => <AboutPage /> } />
// <Route path="/comofunciona"  render={ () => <HowWorksPage /> } />
// <Route path="/contato"  render={ () => <ContactPage /> } />
// <Route path="/atividades"  render={ () => <ActivitiesPage /> } />
// <Route path="/perfil"  render={ () => <ProfilePage /> } />
// <Footer dropdownChange={this.dropdownChange}/>