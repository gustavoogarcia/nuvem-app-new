import React, { Component, Fragment } from 'react';
import gql from "graphql-tag";
import { graphql, compose } from "react-apollo";
import { NameContext } from '../__helpers__/context';
import Page from '../Elements/Layout/Page'
import Container from '../Elements/Layout/Container'
// import Bubbles from '../Components/Bubbles';
import Box from '../Elements/Layout/Box';
import Form from '../Elements/Form/Form';
import InputText from '../Elements/Form/InputText';
import InputRadio from '../Elements/Form/InputRadio';
import InputCheckbox from '../Elements/Form/InputCheckbox';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            register: false,
            email: '',
            password: '',
            rePassword: '',
            birthDate: '',
            userType: '',
            userName: '',
            newsSubscriber: true,
        }
        this.signup = this.signup.bind(this)
        this.login = this.login.bind(this)
    }

    inputChange = (e, input) => {
        this.setState({
            [input]: e.target.value
        })
    }
    
    signup = (e) => {
        e.preventDefault()
        this.props.signup({
            variables: {
                email: e.target.querySelector('input[name="email"]').value,
                password: e.target.querySelector('input[name="password"]').value,
                rePassword: e.target.querySelector('input[name="rePassword"]').value,
                birthDate: e.target.querySelector('input[name="birthDate"]').value,
                userType: e.target.querySelector('input[name="userType"]').value,
                userName: e.target.querySelector('input[name="userName"]').value,
            }
        }).then( res => {
            this.saveUserData(res)
        })
    }

    login = (e) => {
        e.preventDefault()
        this.props.login({
            variables: {
                email: e.target.querySelector('input[name="email"]').value,
                password: e.target.querySelector('input[name="password"]').value,
            }
        }).then( res => {
            this.saveUserData(res)

        })
    }

    saveUserData = (res) => {
        const { jwt } = this.state.register ? res.data.signup : res.data.login
        window.localStorage.setItem("TOKEN", jwt)
        this.props.data.refetch()
    }


    render() {
        const { state, signup, login, inputChange } = this
        const { register, email, password, rePassword, userName, birthDate, userType, newsSubscriber } = state
        const component = "register"
        const userTypes =  [
            { label: "Aluno", value: "student" },
            { label: "Professor", value: "teacher" },
            { label: "Família", value: "family" },
        ]
        
        return (
            <NameContext.Provider value={ component }>
                <Page>
                    <Container>
                        <Box>
                            <h2>{!register ? 'Entrar' : 'Cadastrar'}</h2>
                            <button className='button--social button--facebook'>
                                <FontAwesomeIcon icon={['fab', 'facebook-f']} />
                                Entrar com o Facebook
                            </button>
                            <button className='button--social button--google'>
                                <FontAwesomeIcon icon={['fab', 'google']} />
                                Entrar com o Google
                            </button>
                            <Form onSubmit={ register ? (e) =>  signup(e) : (e) => login(e) }>
                                <InputText 
                                    inputChange={ inputChange }
                                    type="text" 
                                    name="email" 
                                    label="Email*" 
                                    value={ email }/>
                                <InputText 
                                    inputChange={ inputChange }
                                    type="password" 
                                    name="password" 
                                    label="Senha*" 
                                    value={password}/>
                                {register && (
                                    <Fragment>
                                        <InputText 
                                            inputChange={ inputChange }
                                            type="password" 
                                            name="rePassword" 
                                            label="Confirmar senha*" 
                                            value={rePassword}/>
                                        <InputText 
                                            inputChange={ inputChange }
                                            type="number" 
                                            name="birthDate" 
                                            label="Data de nascimento*" 
                                            value={birthDate}/>
                                        <InputText 
                                            inputChange={ inputChange }
                                            type="text" 
                                            name="userName" 
                                            label="Nome de usuário*" 
                                            value={userName}/>
                                        <div className={`${component}__radio`}>
                                            <label>Perfil*</label>
                                            { userTypes.map( ({ label, value }) => (
                                                <InputRadio
                                                    inputChange={ inputChange }
                                                    key={ value } 
                                                    name="userType" 
                                                    label={ label } 
                                                    value={ value }
                                                    checked={ userType }/>
                                            ))}
                                        </div>
                                        <div className={`${component}__checkbox`}>
                                            <InputCheckbox 
                                                inputChange={ inputChange }
                                                name="newsletter" 
                                                label={`Desejo receber as novidades.`} 
                                                value="email"
                                                checked={ newsSubscriber }/>
                                        </div>
                                        <p>Clicando em "Cadastrar", você concorda<br/>com os nossos <a>termos de uso</a>.</p>
                                    </Fragment>
                                )}
                                <button className="button-submit button--orange" >{register ? 'Cadastrar' : 'Entrar'}</button>
                            </Form>            
                        </Box>
                    </Container>
                </Page>
            </NameContext.Provider>
        )
    }
}

const Query = gql`
    {
        currentUser { 
            id
            jwt
            userName
            userPlan
            userType
        }
    }
`

const Signup = gql`
    mutation signup ($email: String!, $password: String!, $userName: String!) {
        signup(email: $email, password: $password, userName: $userName) {
            jwt
        }
    }
`

const Login = gql`
    mutation login ($email: String!, $password: String!) {
        login(email: $email, password: $password) {
            jwt
        }
    }
`

export default compose(
    graphql(Query, { name: "data" }),
    graphql(Signup, { name: 'signup' }),
    graphql(Login, { name: 'login' })
)(Register)