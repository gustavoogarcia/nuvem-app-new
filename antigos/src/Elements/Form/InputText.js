import React, { Fragment } from 'react';

const InputText = ({ inputChange, value, type, name, label, placeholder }) => {
    let input

    return (
        <Fragment>
            <label htmlFor={ name }>{ label }</label>
            <input 
                ref={ node => { input = node } } 
                onChange={(e) => inputChange(e, name )} 
                value={value} type={type} 
                name={ name } 
                placeholder={ placeholder } >
            </input> 
        </Fragment>
    )
}

export default InputText;