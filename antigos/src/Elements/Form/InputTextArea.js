import React, { Fragment } from 'react';

const InputTextArea = ({ name, label, placeholder }) => (
    <Fragment>
        <label htmlFor={ name }>{ label }</label>
        <textarea rows="4" name={ name } placeholder={ placeholder } ></textarea> 
    </Fragment>
);

export default InputTextArea;