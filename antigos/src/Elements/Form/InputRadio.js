import React, { Component, Fragment } from 'react';

class InputText extends Component {
    state = {
        login: true,
        email: '',
        password: '',
        rePassword: '',
        birthDate: '',
        userName: '',
        userType: '',
        newsSubscriber: true,
    }

    render() {
        let input
        const { inputChange, checked, name, label, value} = this.props
        
        return(
            <Fragment>
                <label>
                    { label }
                    <input 
                        ref={ node => { input = node } } 
                        checked={checked === value}
                        type="radio" 
                        name={ name } 
                        value={ value } 
                        onChange={(e) => 
                        inputChange(e, name)}/>
                    <span className="checkmark"></span>
                </label>
            </Fragment>
        )
    }
}

export default InputText;