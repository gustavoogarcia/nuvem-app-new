import React from 'react';

 const Form = ({ children, onSubmit }, { name }) => (
    <form 
        className={`${ name }__form`}
        onSubmit={ onSubmit }>
        { children }
    </form>
)

export default Form

