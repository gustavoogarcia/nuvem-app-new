import React, { Fragment } from 'react';

const InputText = ({ inputChange, checked, name, label, value}) => {
    let input 
    
    return (
        <Fragment>
            <label>
                { label }
                <input 
                    ref={ node => { input = node } } 
                    onChange={(e) => inputChange(e, name)}
                    type="checkbox" 
                    name={ name } 
                    defaultChecked={checked}
                    value={checked}/>
                <span className="checkmark"></span>
            </label>
        </Fragment>
    )
}

export default InputText;