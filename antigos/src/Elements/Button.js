import React from "react";

export default ({ name, label, onClick, className}) => (
    <button className={`button--${name} button--${className}`} onClick={onClick} >{label }</button>
)
