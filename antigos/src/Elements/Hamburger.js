import React from 'react';
import { NameContext } from '../__helpers__/context';
import Box from '../Elements/Layout/Box';

const HeaderHamburguer = ({ isOpen, dropdownChange, location }) => (
    <NameContext.Consumer>
        { name => (
            <NameContext.Provider value={ `hamburger ${name}__hamburger` }>
                <Box isOpen={isOpen} onClick={ (e) => dropdownChange(e, location) }>
                    <div className="hamburger__outer" >
                        <div className="hamburger__inner"></div>
                    </div>
                </Box>
            </NameContext.Provider>
        )}  
    </NameContext.Consumer>
)

export default HeaderHamburguer