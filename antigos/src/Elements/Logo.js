import React from 'react';
import { Link } from 'react-router-dom'
import logo from "../__links__/imgs/nuvem-logo.png";

const Logo = (props, { name }) => (
    <Link to="/"><img className={`logo ${name}__logo`} src={ logo } alt="Nuvem de Estudos Logo"/></Link>
)

export default Logo;