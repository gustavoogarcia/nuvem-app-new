import React from "react";
import { NameContext } from '../../__helpers__/context';

const Page = ({ children }) => (
    <NameContext.Consumer>
        { name => (
            <div className={`page ${name}__page`}>
                { children }
            </div>
        )}  
    </NameContext.Consumer>
)

export default Page