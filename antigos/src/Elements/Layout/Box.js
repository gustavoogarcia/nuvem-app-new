import React from "react";
import { NameContext } from '../../__helpers__/context';

 const Box = ({ children, isOpen, onClick }) => (
    <NameContext.Consumer>
        { name => (
            <div className={`${ name } ${ isOpen ? "is-open" : "" }`}  onClick={ onClick }>
                { children }
            </div>
        )}  
    </NameContext.Consumer>
)   

export default Box