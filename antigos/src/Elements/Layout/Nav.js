import React from "react";
import { NameContext } from '../../__helpers__/context';

 const Nav = ({ children, isOpen, onClick }) => (
    <NameContext.Consumer>
        { name => (
            <div className={ `nav ${ name }__nav ${ isOpen && "is-open" }` }  onClick={ onClick }>
                { children }
            </div>
        )}  
    </NameContext.Consumer>
)   

export default Nav